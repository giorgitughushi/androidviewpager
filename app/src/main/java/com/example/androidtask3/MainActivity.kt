package com.example.androidtask3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager2.widget.ViewPager2
import com.example.androidtask3.Adapters.viewpageradapter
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var tablayout:TabLayout=findViewById(R.id.tablayout);
        var viewpager :ViewPager2=findViewById(R.id.viewpager);

        viewpager.adapter=viewpageradapter(this)
        TabLayoutMediator(tablayout,viewpager){ tab,position->
            tab.text=" Fragment ${position+1}"
        }.attach()











    }
}