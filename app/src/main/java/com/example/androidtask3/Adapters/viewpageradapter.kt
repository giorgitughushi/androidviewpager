package com.example.androidtask3.Adapters

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.androidtask3.Fragments.first_fragment
import com.example.androidtask3.Fragments.second_fragment
import com.example.androidtask3.Fragments.third_fragment

class viewpageradapter(app:AppCompatActivity) :FragmentStateAdapter(app) {
    override fun getItemCount(): Int {

        return 3
    }

    override fun createFragment(position: Int): Fragment {
         if(position==0){
             return first_fragment()
         }
        if(position==1){
            return second_fragment()
        }
        else{
            return third_fragment()
        }









    }
}